package ru.kpfu.itis.minnibaeva;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class WeatherApplication extends Application {
    Stage stage;

    private Scene scene;

    private BorderPane rootLayout;

    private WeatherSearchView weatherSearchView;

    public WeatherSearchView getWeatherSearchView() {
        return weatherSearchView;
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        this.stage.setOnCloseRequest(e -> System.exit(0));

        this.weatherSearchView = new WeatherSearchView();

        this.initLayout();
    }

    private void initLayout() {
        rootLayout = new BorderPane();

        scene = new Scene(rootLayout, 325, 230);
        stage.setScene(scene);
        stage.show();

        this.setView(getWeatherSearchView());
    }

    public void setView(BaseView view) {
        this.stage.setTitle(view.getTitle());
        rootLayout.setCenter(view.getView());
    }
}
