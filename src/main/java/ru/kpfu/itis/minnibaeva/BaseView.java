package ru.kpfu.itis.minnibaeva;

import javafx.scene.Parent;

public abstract class BaseView {

    private static WeatherApplication application;

    public abstract Parent getView();

    public static void setApplication(WeatherApplication application) {
        BaseView.application = application;
    }

    public static WeatherApplication getApplication() throws Exception {
        if (application != null) {
            return application;
        }
        throw new Exception("No Application in BaseView");
    }

    public abstract String getTitle();
}
