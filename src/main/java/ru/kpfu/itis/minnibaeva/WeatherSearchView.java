package ru.kpfu.itis.minnibaeva;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class WeatherSearchView extends BaseView {

    private String title = "Weather";

    private AnchorPane pane = null;

    private HBox hBox;

    private VBox vBox;

    private Button searchButton;
    private TextField cityField;

    private TextArea weatherText;

    SearchWeatherService service = new SearchWeatherServiceImpl();

    private final EventHandler<ActionEvent> searchEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (searchButton == actionEvent.getSource()) {
                String city = cityField.getText();

                StringBuilder jsonString = new StringBuilder(service.getWeatherJSON("https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=b684cfe1558a37f5cab1c97d60108160"));
                Map<String, Object> json = null;
                try {
                    json = service.parseGson(jsonString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                weatherText.setText("");
                for (String header : json.keySet()) {
                    switch (header) {
                        case ("main pressure"):
                            weatherText.setText("Давление" + ": " + json.get(header) + " па\n" + weatherText.getText());
                            break;
                        case ("name"):
                            weatherText.setText("Город" + ": " + json.get(header) + "\n" + weatherText.getText());
                            break;
                        case ("main temp_max"):
                            weatherText.setText("Максимальная температура" + ": " + String.format("%.1f", ((double) json.get(header) - 273.15)) + " градус\n" + weatherText.getText());
                            break;
                        case ("main temp_min"):
                            weatherText.setText("Минимальная температура" + ": " + String.format("%.1f", ((double) json.get(header) - 273.15)) + " градус\n" + weatherText.getText());
                            break;
                        case ("main temp"):
                            weatherText.setText("Температура" + ": " + String.format("%.1f", ((double) json.get(header) - 273.15)) + " градус\n" + weatherText.getText());
                            break;
                        case ("main feels_like"):
                            weatherText.setText("Ощущается как" + ": " + String.format("%.1f", ((double) json.get(header) - 273.15)) + " градус\n" + weatherText.getText());
                            break;
                        case ("weather description"):
                            weatherText.setText("Погода" + ": " + json.get(header) + "\n" + weatherText.getText());
                            break;
                        case ("wind deg"):
                            weatherText.setText("Направление ветра" + ": " + json.get(header) + " градуса \n" + weatherText.getText());
                            break;
                        case ("main humidity"):
                            weatherText.setText("Влажность" + ": " + json.get(header) + "\n" + weatherText.getText());
                            break;
                        case ("wind speed"):
                            weatherText.setText("Скорость ветра" + ": " + json.get(header) + " м\\с \n" + weatherText.getText());
                            break;
                        case ("visibility"):
                            weatherText.setText("Видимость" + ": " + json.get(header) + " м\n" + weatherText.getText());
                            break;
                    }
                }
                weatherText.setText(weatherText.getText(0, weatherText.getText().length() - 1));
            }
        }
    };

    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }

    @Override
    public String getTitle() {
        return title;
    }

    private void createView() {
        pane = new AnchorPane();

        hBox = new HBox(5);
        vBox = new VBox(5);

        searchButton = new Button("поиск");
        searchButton.setOnAction(searchEvent);
        cityField = new TextField();
        cityField.setPromptText("Введите город");

        weatherText = new TextArea();
        weatherText.setEditable(false);
        weatherText.setWrapText(true);
        weatherText.setMaxWidth(300);
        weatherText.setMinWidth(300);
        hBox.getChildren().addAll(cityField, searchButton);

        vBox.getChildren().addAll(hBox, weatherText);

        pane.getChildren().add(vBox);

        AnchorPane.setTopAnchor(vBox, 5.0);
        AnchorPane.setLeftAnchor(vBox, 10.0);
        AnchorPane.setRightAnchor(vBox, 10.0);

    }
}
