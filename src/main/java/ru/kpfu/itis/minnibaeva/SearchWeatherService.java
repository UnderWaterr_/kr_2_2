package ru.kpfu.itis.minnibaeva;

import java.io.IOException;
import java.util.Map;

public interface SearchWeatherService {
    String getWeatherJSON(String url);
    Map<String, Object> parseGson(StringBuilder jsonString) throws IOException;
}
